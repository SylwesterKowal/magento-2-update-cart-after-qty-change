<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\UpdateCartAfterQtyChange\Block;

class Cart extends \Magento\Framework\View\Element\Template
{

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Kowal\UpdateCartAfterQtyChange\Helper\Data $dataHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Kowal\UpdateCartAfterQtyChange\Helper\Data      $dataHelper,
        array                                            $data = []
    )
    {
        $this->dataHelper = $dataHelper;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function enable()
    {
        if ($this->dataHelper->getGeneralCfg('enable')) {
            return true;
        } else {
            return false;
        }
    }
}

